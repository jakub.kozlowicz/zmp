#include <chrono>
#include <iostream>
#include <memory>
#include <sstream>
#include <thread>

#include "interpreter_rotate.hh"
#include "mobile_object.hh"

#include "communication/sender.hh"
#include "scene/scene.hh"

extern "C" {
Interpreter4Command* create_command(void);

const char* get_command_name() {
    return "Rotate";
}

} /* extern "C" */

Interpreter4Command* create_command(void) {
    return Interpreter4Rotate::create_command();
}

Interpreter4Rotate::Interpreter4Rotate() : _speed_deg(0), _angle_deg(0) {}

void Interpreter4Rotate::print_command() const {
    std::cout << get_command_name() << " " << _name << " " << _speed_deg << " " << _angle_deg << "\n";
}

const char* Interpreter4Rotate::get_command_name() const {
    return ::get_command_name();
}

bool Interpreter4Rotate::exec_command(std::shared_ptr<Scene> scene, std::shared_ptr<Sender> sender) const {
    std::shared_ptr<MobileObject> object;
    try {
        object = scene->find_mobile_object(_name);
    } catch (const std::out_of_range& e) {
        std::cerr << e.what() << "\n";
        return false;
    }

    object->lock();

    Vector3D current_rotation, rotation, new_rotation;
    double phi, ksi, theta;
    current_rotation = object->get_angles_deg();

    object->unlock();

    phi = current_rotation[0] * 3.14 / 180;
    ksi = current_rotation[1] * 3.14 / 180;
    theta = current_rotation[2] * 3.14 / 180;

    rotation[0] = (cos(phi) * sin(ksi) * cos(theta) + sin(phi) * sin(theta)) * _angle_deg;
    rotation[1] = (cos(phi) * sin(ksi) * sin(theta) - sin(phi) * cos(theta)) * _angle_deg;
    rotation[2] = cos(phi) * cos(ksi) * _angle_deg;

    if (_speed_deg == 0) {
        throw std::runtime_error("Speed cannot be zero");
    }

    double time = _angle_deg / _speed_deg;
    if (time < 0) {
        rotation[2] *= -1;
    }
    time = std::abs(time);

    for (int i = 0; i < 100; ++i) {
        object->lock();
        // Vector3D operators seems to be not working :(
        new_rotation[0] = current_rotation[0] + (rotation[0] / 100.0) * (double) i;
        new_rotation[1] = current_rotation[1] + (rotation[1] / 100.0) * (double) i;
        new_rotation[2] = current_rotation[2] + (rotation[2] / 100.0) * (double) i;
        object->set_angles_deg(new_rotation);
        object->unlock();

        std::ostringstream ss;
        ss << "UpdateObj Name=" << object->get_name() << " RotXYZ_deg=" << new_rotation << "\n";

        try {
            sender->send(ss.str().c_str());
        } catch (const std::runtime_error& e) {
            std::cerr << e.what() << "\n";
            return false;
        }

        std::this_thread::sleep_for(std::chrono::duration<double>(time / 100.0));
    }

    return true;
}

bool Interpreter4Rotate::read_params(std::istream& commands_stream) {
    commands_stream >> _name >> _speed_deg >> _angle_deg;

    if (commands_stream.fail()) {
        return false;
    }

    return true;
}

void Interpreter4Rotate::print_params() const {
    std::cout << "Name of the object: " << _name << " Speed [deg/s]: " << _speed_deg << " Angle [m]: " << _angle_deg
              << "\n";
}

Interpreter4Command* Interpreter4Rotate::create_command() {
    return new Interpreter4Rotate();
}

void Interpreter4Rotate::print_syntax() const {
    std::cout << get_command_name() << " | Name of the object | Speed [deg/s] | Angle [deg]\n";
}
