#include <chrono>
#include <iostream>
#include <memory>
#include <sstream>
#include <thread>

#include "interpreter_move.hh"
#include "mobile_object.hh"

#include "communication/sender.hh"
#include "scene/scene.hh"

extern "C" {
Interpreter4Command* create_command(void);

const char* get_command_name() {
    return "Move";
}

} /* extern "C" */

Interpreter4Command* create_command(void) {
    return Interpreter4Move::create_command();
}

Interpreter4Move::Interpreter4Move() : _speed_ms(0), _distance_m(0) {}

void Interpreter4Move::print_command() const {
    std::cout << get_command_name() << " " << _name << " " << _speed_ms << " " << _distance_m << "\n";
}

const char* Interpreter4Move::get_command_name() const {
    return ::get_command_name();
}

bool Interpreter4Move::exec_command(std::shared_ptr<Scene> scene, std::shared_ptr<Sender> sender) const {
    std::shared_ptr<MobileObject> object;
    try {
        object = scene->find_mobile_object(_name);
    } catch (const std::out_of_range& e) {
        std::cerr << e.what() << "\n";
        return false;
    }

    object->lock();
    Vector3D translation, current_rotation;
    current_rotation = object->get_angles_deg() * 3.14 / 180;
    object->unlock();

    translation[0] = (cos(current_rotation[0]) * sin(current_rotation[1]) * cos(current_rotation[2]) +
                      sin(current_rotation[0]) * sin(current_rotation[2])) *
                     _distance_m;
    translation[1] = (cos(current_rotation[0]) * sin(current_rotation[1]) * sin(current_rotation[2]) -
                      sin(current_rotation[0]) * cos(current_rotation[2])) *
                     _distance_m;
    translation[2] = cos(current_rotation[0]) * cos(current_rotation[1]) * _distance_m;

    if (_speed_ms == 0) {
        throw std::runtime_error("Speed cannot be zero");
    }

    double time = _distance_m / _speed_ms;
    if (time < 0) {
        translation *= -1;
    }

    time = std::abs(time);

    Vector3D new_position;
    for (int i = 0; i < 100; ++i) {
        object->lock();
        // Vector3D operators seems to be not working :(
        auto current_position = object->get_position_m();
        new_position[0] = current_position[0] + (translation[0] / 100.0);
        new_position[1] = current_position[1] + (translation[1] / 100.0);
        new_position[2] = current_position[2] + (translation[2] / 100.0);
        object->set_position_m(new_position);
        object->unlock();

        std::ostringstream ss;
        ss << "UpdateObj Name=" << object->get_name() << " Trans_m=" << new_position << "\n";

        try {
            sender->send(ss.str().c_str());
        } catch (const std::runtime_error& e) {
            std::cerr << e.what() << "\n";
            return false;
        }

        std::this_thread::sleep_for(std::chrono::duration<double>(time / 100.0));
    }

    return true;
}

bool Interpreter4Move::read_params(std::istream& commands_stream) {
    commands_stream >> _name >> _speed_ms >> _distance_m;

    if (commands_stream.fail()) {
        return false;
    }

    return true;
}

void Interpreter4Move::print_params() const {
    std::cout << "Name of the object: " << _name << " Velocity [m/s]: " << _speed_ms << " Distance [m]: " << _distance_m
              << "\n";
}

Interpreter4Command* Interpreter4Move::create_command() {
    return new Interpreter4Move();
}

void Interpreter4Move::print_syntax() const {
    std::cout << get_command_name() << " | Name of the object | Velocity [m/s] | Distance [m]\n";
}
