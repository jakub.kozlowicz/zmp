#include <chrono>
#include <iostream>
#include <thread>

#include "interpreter_pause.hh"

#include "communication/sender.hh"
#include "scene/scene.hh"

extern "C" {
Interpreter4Command* create_command(void);

const char* get_command_name() {
    return "Pause";
}

} /* extern "C" */

Interpreter4Command* create_command(void) {
    return Interpreter4Pause::create_command();
}

Interpreter4Pause::Interpreter4Pause() : _duration_ms(0) {}

void Interpreter4Pause::print_command() const {
    std::cout << get_command_name() << " " << _duration_ms << "\n";
}

const char* Interpreter4Pause::get_command_name() const {
    return ::get_command_name();
}

bool Interpreter4Pause::exec_command(std::shared_ptr<Scene> scene, std::shared_ptr<Sender> sender) const {
    std::this_thread::sleep_for(std::chrono::duration<double, std::milli>(_duration_ms));
    return true;
}

bool Interpreter4Pause::read_params(std::istream& commands_stream) {
    commands_stream >> _duration_ms;

    if (commands_stream.fail()) {
        return false;
    }

    return true;
}

void Interpreter4Pause::print_params() const {
    std::cout << "Duration [s]: " << _duration_ms << "\n";
}

Interpreter4Command* Interpreter4Pause::create_command() {
    return new Interpreter4Pause();
}

void Interpreter4Pause::print_syntax() const {
    std::cout << get_command_name() << " | Pause duration [ms]\n";
}
