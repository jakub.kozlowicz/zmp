#include <iostream>
#include <sstream>

#include "interpreter_set.hh"
#include "mobile_object.hh"

#include "communication/sender.hh"
#include "scene/scene.hh"

extern "C" {
Interpreter4Command* create_command(void);

const char* get_command_name() {
    return "Set";
}

} /* extern "C" */


Interpreter4Command* create_command(void) {
    return Interpreter4Set::create_command();
}

void Interpreter4Set::print_command() const {
    std::cout << get_command_name() << " " << _name << " " << _position_m[0] << " " << _position_m[1] << " "
              << _position_m[2] << "\n";
}

const char* Interpreter4Set::get_command_name() const {
    return ::get_command_name();
}

bool Interpreter4Set::exec_command(std::shared_ptr<Scene> scene, std::shared_ptr<Sender> sender) const {
    std::shared_ptr<MobileObject> object;
    try {
        object = scene->find_mobile_object(_name);
    } catch (const std::out_of_range& e) {
        std::cerr << e.what() << "\n";
        return false;
    }

    object->lock();
    auto current_position = object->get_position_m();
    current_position = _position_m;

    object->set_position_m(current_position);
    object->unlock();

    std::ostringstream ss;
    ss << "UpdateObj Name=" << object->get_name() << " Trans_m=" << current_position << "\n";

    try {
        sender->send(ss.str().c_str());
    } catch (const std::runtime_error& e) {
        std::cerr << e.what() << "\n";
        return false;
    }

    return true;
}

bool Interpreter4Set::read_params(std::istream& commands_stream) {
    commands_stream >> _name >> _position_m[0] >> _position_m[1] >> _position_m[2];

    if (commands_stream.fail()) {
        return false;
    }

    return true;
}

void Interpreter4Set::print_params() const {
    std::cout << "Name of the object: " << _name << " X Distance [m]: " << _position_m[0]
              << " Y Distance [m]: " << _position_m[1] << " Z Distance [m]: " << _position_m[2] << "\n";
}

Interpreter4Command* Interpreter4Set::create_command() {
    return new Interpreter4Set();
}

void Interpreter4Set::print_syntax() const {
    std::cout << get_command_name() << " | Name of the object | X Distance [m] | Y Distance [m] | Z Distance [m]\n";
}
