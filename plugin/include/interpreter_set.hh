#ifndef INTERPRETER_SET_HH
#define INTERPRETER_SET_HH

#ifndef __GNUG__
#pragma interface
#pragma implementation
#endif

#include <memory>
#include <string>

#include <vector_3D.hh>

#include "interpreter_command.hh"

#include "communication/sender.hh"
#include "scene/scene.hh"

class Interpreter4Set : public Interpreter4Command {
    Vector3D _position_m;
    std::string _name;

public:
    Interpreter4Set() = default;

    void print_command() const override;

    void print_syntax() const override;

    [[nodiscard]] const char* get_command_name() const override;

    [[nodiscard]] bool exec_command(std::shared_ptr<Scene> scene, std::shared_ptr<Sender> sender) const override;

    bool read_params(std::istream& commands_stream) override;

    void print_params() const;

    static Interpreter4Command* create_command();
};


#endif /* INTERPRETER_SET_HH */