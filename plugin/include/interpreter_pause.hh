#ifndef INTERPRETER_PAUSE_HH
#define INTERPRETER_PAUSE_HH

#ifndef __GNUG__
#pragma interface
#pragma implementation
#endif

#include <string>

#include "interpreter_command.hh"

class Interpreter4Pause : public Interpreter4Command {
    double _duration_ms;

public:
    Interpreter4Pause();

    void print_command() const override;

    void print_syntax() const override;

    [[nodiscard]] const char* get_command_name() const override;

    [[nodiscard]] bool exec_command(std::shared_ptr<Scene> scene, std::shared_ptr<Sender> sender) const override;

    bool read_params(std::istream& commands_stream) override;

    void print_params() const;

    static Interpreter4Command* create_command();
};


#endif /* INTERPRETER_PAUSE_HH */