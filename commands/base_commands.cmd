#define ROTATE_SPEED     30
/*
 *  Przykładowy zestaw poleceń
 */
  Rotate Podstawa ROTATE_SPEED 40
  Pause 1000 /* Zawieszenie na 1 sek. */
  Move  Podstawa.Ramie1  50 1
  Rotate Podstawa ROTATE_SPEED 30 /* Rotate i Move wykonywane razem */
  Move  Podstawa 50 1       /* powoduja jazde po luku         */
  Rotate Podstawa.Ramie1 ROTATE_SPEED -60
  // Rotate Podstawa.Ramie1.Ramie2 ROTATE_SPEED -60
  Move Podstawa.Ramie1.Ramie2 100 2
