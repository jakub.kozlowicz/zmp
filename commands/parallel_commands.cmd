#define ROTATE_SPEED     30
/*
 *  Przykładowy zestaw poleceń
 */
Begin_Parallel_Actions
 Set   Podstawa 0 0 0                 // Polozenie podstawy wzgledem (0, 0, 0)
 // Set   Podstawa.Ramie1 0 0 0          // Polozenie pierwszego przegubu wzgledem podstawy
 // Set   Podstawa.Ramie1.Ramie2 0 0 0   // Polozenie drugiego przegubu wzgledem pierwszego
End_Parallel_Actions

Begin_Parallel_Actions
 Rotate Podstawa.Ramie1.Ramie2 ROTATE_SPEED 40
End_Parallel_Actions

Begin_Parallel_Actions  Pause 1000 /* Zawieszenie na 1 sek. */ End_Parallel_Actions

Begin_Parallel_Actions
 Move  Podstawa  5 5
 Rotate Podstawa.Ramie1 ROTATE_SPEED 90 /* Rotate i Move wykonywane razem */
 Move  Podstawa.Ramie1 5 2            /* powoduja jazde po luku         */
End_Parallel_Actions