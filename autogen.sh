#!/bin/sh

# Copyright (c) 2022 Jakub Kozłowicz
# Script to generate automake files.
# Adapted from libzmq project.

if ! command -v libtoolize >/dev/null 2>&1; then
    if ! command -v libtool >/dev/null 2>&1; then
        echo "autogen.sh: error: could not find libtool.  libtool is required to run autogen.sh." 1>&2
        exit 1
    fi
fi

if ! command -v autoreconf >/dev/null 2>&1; then
    echo "autogen.sh: error: could not find autoreconf.  autoconf and automake are required to run autogen.sh." 1>&2
    exit 1
fi

if ! mkdir -p ./config; then
    echo "autogen.sh: error: could not create directory: ./config." 1>&2
    exit 1
fi

autoreconf --install --force --verbose -I config
res=$?
if [ "$res" -ne 0 ]; then
    echo "autogen.sh: error: autoreconf exited with status $res" 1>&2
    exit 1
fi
