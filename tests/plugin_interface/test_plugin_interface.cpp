#include <utility>
#include <vector>

#include <gtest/gtest.h>

#include "plugin/plugin_interface.hh"

const std::vector<std::string> DEFINED_PLUGIN_COMMANDS = {"Move", "Rotate", "Set", "Pause"};

TEST(LoadUndefinedPlugin, test) {
    auto plugin_interface = PluginInterface("undefined_command");
    EXPECT_THROW(
            {
                try {
                    plugin_interface.load_plugin();
                } catch (const std::runtime_error& e) {
                    throw;
                }
            },
            std::runtime_error);
}

TEST(UnloadNotPreviouslyLoadedPlugin, test) {
    auto plugin_interface = PluginInterface("undefined_command");
    EXPECT_THROW(
            {
                try {
                    plugin_interface.unload_plugin();
                } catch (const std::runtime_error& e) {
                    throw;
                }
            },
            std::runtime_error);
}

TEST(LoadAllDefinedPlugins, test) {
    for (auto&& command: DEFINED_PLUGIN_COMMANDS) {
        auto plugin_interface = PluginInterface(command);
        EXPECT_NO_THROW(plugin_interface.load_plugin());
    }
}

TEST(CheckCurrentCommandCorrect, test) {
    auto plugin_interface = PluginInterface("Move");
    EXPECT_EQ(plugin_interface.get_current_command(), "Move");
}

TEST(ChangeCommandNamePluginInterfaceInstance, test) {
    auto plugin_interface = PluginInterface("Move");
    EXPECT_EQ(plugin_interface.get_current_command(), "Move");
    plugin_interface.set_new_command("Pause");
    EXPECT_EQ(plugin_interface.get_current_command(), "Pause");
}