#include <sstream>

#include <gtest/gtest.h>

#include "interpreter_set.hh"

TEST(GetCommandNameSet, test) {
    auto interpreter = Interpreter4Set();
    EXPECT_EQ(std::string(interpreter.get_command_name()), "Set");
}

TEST(ReadParametersCommandSet, test) {
    std::stringstream ss;
    std::string name = "Ob_A";
    int x_m = 10;
    int y_m = 20;
    int z_m = 30;
    ss << name << " " << x_m << " " << y_m << " " << z_m;

    auto interpreter = Interpreter4Set();
    EXPECT_TRUE(interpreter.read_params(ss));
}
