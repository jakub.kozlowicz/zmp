#include <sstream>

#include <gtest/gtest.h>

#include "interpreter_move.hh"

TEST(ExpectEqualCommandName, test) {
    auto interpreter = Interpreter4Move();
    EXPECT_EQ(std::string(interpreter.get_command_name()), "Move");
}

TEST(ReadParametersCommandMove, test) {
    std::stringstream ss;
    std::string name = "Ob_A";
    int speed_ms = 10;
    int distance_m = 20;
    ss << name << " " << speed_ms << " " << distance_m;

    auto interpreter = Interpreter4Move();
    EXPECT_TRUE(interpreter.read_params(ss));
}

TEST(CapturePrintSyntaxCommandMove, test) {
    const std::string ref = "Move | Name of the object | Velocity [m/s] | Distance [m]\n";
    auto interpreter = Interpreter4Move();
    testing::internal::CaptureStdout();
    interpreter.print_syntax();
    std::string output = testing::internal::GetCapturedStdout();
    EXPECT_EQ(output, ref);
}
