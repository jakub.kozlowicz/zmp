cmake_minimum_required(VERSION 3.16)

include(FetchContent)

message(CHECK_START "Fetching Google Test")
FetchContent_Declare(
  googletest
  GIT_REPOSITORY "https://github.com/google/googletest.git"
  GIT_TAG release-1.12.1)

FetchContent_MakeAvailable(googletest)
message(CHECK_PASS "fetched")

add_library(test_main OBJECT test_main.cpp)
target_link_libraries(test_main GTest::gtest_main)

include(GoogleTest)

set(Targets "")
list(
  APPEND
  Targets
  command_reader
  interpreter_move
  interpreter_pause
  interpreter_rotate
  interpreter_set
  plugin_interface)

foreach(target ${Targets})
  add_executable(test_${target} ${target}/test_${target}.cpp
                                $<TARGET_OBJECTS:test_main>)
  target_link_libraries(test_${target} GTest::gtest ${target})
  add_test(NAME test_${target} COMMAND test_${target})
endforeach()

target_compile_definitions(
  test_command_reader
  PRIVATE COMMANDS_DIR="${CMAKE_CURRENT_SOURCE_DIR}/command_reader/")
