#include <sstream>

#include <gtest/gtest.h>

#include "interpreter_rotate.hh"

TEST(GetCommandName, test) {
    auto interpreter = Interpreter4Rotate();
    EXPECT_EQ(std::string(interpreter.get_command_name()), "Rotate");
}

TEST(ReadParametersCommandRotate, test) {
    std::stringstream ss;
    std::string name = "Ob_A";
    int speed_ms = 10;
    int angle_deg = 20;
    ss << name << " " << speed_ms << " " << angle_deg;

    auto interpreter = Interpreter4Rotate();
    EXPECT_TRUE(interpreter.read_params(ss));
}
