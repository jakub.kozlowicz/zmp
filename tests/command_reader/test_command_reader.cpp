#include <filesystem>
#include <sstream>

#include <gtest/gtest.h>

#include "command_reader.hh"

const std::filesystem::path commands_dir{COMMANDS_DIR};

TEST(CheckCommandReader, test) {
    std::stringstream ss, ref_ss, check_ss;
    std::string word;
    CommandReader::read_commands(commands_dir / "test_commands.cmd", ss);

    ref_ss << "Set Ob_A 2 0 30 ";
    ref_ss << "Set Ob_B 10 10 0 ";
    ref_ss << "Rotate Ob_B 30 40 ";
    ref_ss << "Pause 1000 ";
    ref_ss << "Move Ob_A 10 10 ";
    ref_ss << "Rotate Ob_B 30 60 ";
    ref_ss << "Move Ob_B 10 20 ";

    while (ss >> word) {
        check_ss << word << " ";
    }
    EXPECT_EQ(check_ss.str(), ref_ss.str());
}

TEST(Exception, HasCertainMessage) {
    std::stringstream ss;
    // Proces will be open anyway because there is "cpp" program in path.
    EXPECT_THROW(
            {
                try {
                    CommandReader::read_commands("invalid_file.cmd", ss);
                } catch (const std::runtime_error& e) {
                    EXPECT_STREQ("Error closing preprocessor process.", e.what());
                    throw;
                }
            },
            std::runtime_error);
}