#include <sstream>

#include <gtest/gtest.h>

#include "interpreter_pause.hh"

TEST(GetCommandInterpreterName, test) {
    auto interpreter = Interpreter4Pause();
    EXPECT_EQ(std::string(interpreter.get_command_name()), "Pause");
}

TEST(ReadParametersCommandPause, test) {
    std::stringstream ss;
    int time_ms = 10;
    ss << time_ms;

    auto interpreter = Interpreter4Pause();
    EXPECT_TRUE(interpreter.read_params(ss));
}
