#include <memory>
#include <stdexcept>
#include <utility>

#include <dlfcn.h>

#include "interpreter_command.hh"
#include "plugin/plugin_interface.hh"

using CreateCommandInterpreterFunction = Interpreter4Command* (*) ();

PluginInterface::PluginInterface(std::string command_name) : _library_handle(nullptr) {
    set_new_command(std::move(command_name));
}

void PluginInterface::load_plugin() {
    _library_handle = dlopen(_library_name.c_str(), RTLD_LAZY);
    if (!_library_handle) {
        throw std::runtime_error(dlerror());
    }

    void* function_symbol = dlsym(_library_handle, "create_command");
    if (!function_symbol) {
        throw std::runtime_error(dlerror());
    }

    const auto create_command_function = reinterpret_cast<CreateCommandInterpreterFunction>(function_symbol);
    _command_interpreter = std::shared_ptr<Interpreter4Command>(create_command_function());
}

void PluginInterface::unload_plugin() {
    _command_interpreter.reset();

    const auto res = dlclose(_library_handle);
    if (res) {
        throw std::runtime_error(dlerror());
    }

    _library_handle = nullptr;
}

void PluginInterface::set_new_command(std::string command_name) {
    _command_name = std::move(command_name);

#ifdef __APPLE__
    _library_name = "libInterp4" + _command_name + ".dylib";
#endif

#ifdef __linux__
    _library_name = "libInterp4" + _command_name + ".so";
#endif

    _library_handle = nullptr;
    _command_interpreter = nullptr;
}

std::string PluginInterface::get_current_command() {
    return _command_name;
}
