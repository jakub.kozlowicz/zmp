#include <filesystem>
#include <iostream>

#include <xercesc/sax2/DefaultHandler.hpp>
#include <xercesc/sax2/SAX2XMLReader.hpp>
#include <xercesc/sax2/XMLReaderFactory.hpp>
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>

#include "config/config_reader.hh"

ConfigReader::ConfigReader(Configuration& config) : _config(config) {}

void ConfigReader::startDocument() {}

void ConfigReader::endDocument() {}

void ConfigReader::startElement(const XMLCh* const uri, const XMLCh* const local_name, const XMLCh* const q_name,
                                const xercesc::Attributes& attributes) {
    char* element_name = xercesc::XMLString::transcode(local_name);
    start_element(element_name, attributes);
    xercesc::XMLString::release(&element_name);
}

void ConfigReader::endElement(const XMLCh* const uri, const XMLCh* const local_name, const XMLCh* const q_name) {}

void ConfigReader::fatalError(const xercesc::SAXParseException& e) {
    char* message = xercesc::XMLString::transcode(e.getMessage());
    char* file = xercesc::XMLString::transcode(e.getSystemId());

    std::ostringstream exc;
    exc << "SAXParseException! File: " << file << " Line: " << e.getLineNumber() << " Column: " << e.getColumnNumber()
        << " Message: " << message;

    xercesc::XMLString::release(&message);
    xercesc::XMLString::release(&file);

    throw std::runtime_error(exc.str());
}

void ConfigReader::error(const xercesc::SAXParseException& e) {
    char* message = xercesc::XMLString::transcode(e.getMessage());
    std::cerr << "Error: " << message << std::endl;
    xercesc::XMLString::release(&message);
}

void ConfigReader::warning(const xercesc::SAXParseException& e) {
    char* message = xercesc::XMLString::transcode(e.getMessage());
    std::cerr << "Warning: " << message << std::endl;
    xercesc::XMLString::release(&message);
}

void ConfigReader::start_element(const std::string& element_name, const xercesc::Attributes& attributes) {
    if (element_name == "Lib") {
        process_lib_attributes(attributes);
        return;
    }

    if (element_name == "Cube") {
        process_cube_attributes(attributes);
        return;
    }
}

void ConfigReader::process_lib_attributes(const xercesc::Attributes& attributes) {
    if (attributes.getLength() != 1) {
        throw std::runtime_error("Wrong number of attributes for \"Lib\" element. Got " +
                                 std::to_string(attributes.getLength()) + " expected 1");
    }

    char* parameter_name = xercesc::XMLString::transcode(attributes.getQName(0));

    if (strcmp(parameter_name, "Name") != 0) {
        std::ostringstream exc;
        exc << "Wrong name for \"Lib\" attribute. Got " << parameter_name << " Expected \"Name\"";
        throw std::runtime_error(exc.str());
    }

    XMLSize_t index = 0;
    char* lib_name = xercesc::XMLString::transcode(attributes.getValue(index));

    _config.add_lib(lib_name);

    xercesc::XMLString::release(&parameter_name);
    xercesc::XMLString::release(&lib_name);
}

void ConfigReader::process_cube_attributes(const xercesc::Attributes& attributes) {
    const auto attributes_number = attributes.getLength();
    if (attributes_number < 1) {
        throw std::runtime_error("Wrong number of attributes for \"Cube\" element. Got " +
                                 std::to_string(attributes_number) + " expected >= 1");
    }

    Configuration::Cube cube;
    for (XMLSize_t index = 0; index < attributes_number; ++index) {
        char* attribute_name = xercesc::XMLString::transcode(attributes.getQName(index));
        char* attribute_value = xercesc::XMLString::transcode(attributes.getValue(index));

        if (strcmp(attribute_name, "Name") == 0) {
            cube.name = attribute_value;
        }

        if (strcmp(attribute_name, "Shift") == 0) {
            std::istringstream ss;
            ss.str(attribute_value);
            cube.shift = Configuration::read_vector_parameters(ss);
        }

        if (strcmp(attribute_name, "Scale") == 0) {
            std::istringstream ss;
            ss.str(attribute_value);
            cube.scale = Configuration::read_vector_parameters(ss);
        }

        if (strcmp(attribute_name, "Trans_m") == 0) {
            std::istringstream ss;
            ss.str(attribute_value);
            cube.translation_m = Configuration::read_vector_parameters(ss);
        }

        if (strcmp(attribute_name, "RotXYZ_deg") == 0) {
            std::istringstream ss;
            ss.str(attribute_value);
            cube.rotation_deg = Configuration::read_vector_parameters(ss);
        }

        if (strcmp(attribute_name, "RGB") == 0) {
            std::istringstream ss;
            ss.str(attribute_value);
            cube.color_rgb = Configuration::read_vector_parameters(ss);
        }

        xercesc::XMLString::release(&attribute_name);
        xercesc::XMLString::release(&attribute_value);
    }

    _config.add_cube(cube);
}


void ConfigReader::parse_file(const std::filesystem::path& config_file, Configuration& config) {
    try {
        xercesc::XMLPlatformUtils::Initialize();
    } catch (const xercesc::XMLException& e) {
        char* message = xercesc::XMLString::transcode(e.getMessage());
        std::string exc = "Error during XML initialization! Exception message: " + std::string(message);
        xercesc::XMLString::release(&message);
        throw std::runtime_error(exc);
    }

    xercesc::SAX2XMLReader* parser = xercesc::XMLReaderFactory::createXMLReader();

    for (auto feature: {xercesc::XMLUni::fgSAX2CoreNameSpaces, xercesc::XMLUni::fgSAX2CoreValidation,
                        xercesc::XMLUni::fgXercesDynamic, xercesc::XMLUni::fgXercesSchema,
                        xercesc::XMLUni::fgXercesSchemaFullChecking, xercesc::XMLUni::fgXercesValidationErrorAsFatal}) {
        parser->setFeature(feature, true);
    }

    DefaultHandler* handler = new ConfigReader(config);
    parser->setContentHandler(handler);
    parser->setErrorHandler(handler);

    try {
        std::ostringstream ss;
        ss << (config_file.parent_path() / config_file.stem()).c_str() << ".xsd";
        if (!parser->loadGrammar(ss.str().c_str(), xercesc::Grammar::SchemaGrammarType, true)) {
            throw std::runtime_error("Error reading grammar file for XML");
        }
        parser->setFeature(xercesc::XMLUni::fgXercesUseCachedGrammarInParse, true);
        parser->parse(config_file.c_str());
    } catch (const xercesc::XMLException& e) {
        char* message = xercesc::XMLString::transcode(e.getMessage());
        const auto exc = "Exception occured during parsing file! " + std::string(message);
        xercesc::XMLString::release(&message);
        throw std::runtime_error(exc);
    } catch (const xercesc::SAXParseException& e) {
        char* message = xercesc::XMLString::transcode(e.getMessage());
        char* file = xercesc::XMLString::transcode(e.getSystemId());

        std::ostringstream exc;
        exc << "SAXParseException! File: " << file << " Line: " << e.getLineNumber()
            << " Column: " << e.getColumnNumber() << " Message: " << message;

        xercesc::XMLString::release(&message);
        xercesc::XMLString::release(&file);

        throw std::runtime_error(exc.str());
    } catch (...) {
        delete parser;
        delete handler;
        throw;
    }

    delete parser;
    delete handler;
}