#include <memory>

#include "scene/scene.hh"

std::shared_ptr<MobileObject> Scene::find_mobile_object(const std::string& object_name) {
    std::scoped_lock lock(_guard);

    return _objects.at(object_name);
}

void Scene::add_mobile_object(const std::shared_ptr<MobileObject>& mobile_object) {
    std::scoped_lock lock(_guard);

    _objects.emplace(mobile_object->get_name(), mobile_object);
}
