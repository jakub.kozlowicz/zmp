#include <arpa/inet.h>
#include <cstdio>
#include <cstring>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include "communication/sender.hh"

Sender::Sender(std::string server_address, int socket_number)
    : _socket_number(socket_number), _socket_fd(0), _server_address(std::move(server_address)) {}

bool Sender::open_connection() {
    std::scoped_lock lock(_guard);

    struct sockaddr_in address_data {};

    bzero((char*) &address_data, sizeof(address_data));

    address_data.sin_family = AF_INET;
    address_data.sin_addr.s_addr = inet_addr(_server_address.c_str());
    address_data.sin_port = htons(_socket_number);

    if ((_socket_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        perror("Error creating socket to connect to server");
        return false;
    }

    if (connect(_socket_fd, (struct sockaddr*) &address_data, sizeof(address_data)) < 0) {
        perror("Cannot connect to server on given port");
        return false;
    }

    return true;
}

void Sender::send(const char* message) {
    std::scoped_lock lock(_guard);

    ssize_t sent_count;
    auto to_send = (ssize_t) strlen(message);
    while ((sent_count = write(_socket_fd, message, to_send)) > 0) {
        to_send -= sent_count;
        message += sent_count;
    }
    if (sent_count < 0) {
        throw std::runtime_error("Error sending message to server!");
    }
}

bool Sender::close_connection() {
    std::scoped_lock lock(_guard);

    if (close(_socket_fd) < 0) {
        return false;
    }

    return true;
}