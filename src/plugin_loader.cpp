#include "plugin/plugin_loader.hh"

void PluginLoader::unload() {
    for (auto&& plugin: _collection) {
        plugin->unload_plugin();
    }
}

std::shared_ptr<PluginInterface> PluginLoader::request(const std::string& command_name) {
    auto plugin = std::make_shared<PluginInterface>(command_name);
    plugin->load_plugin();
    _collection.emplace_back(plugin);
    return plugin;
}
