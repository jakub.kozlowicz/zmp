#include <list>
#include <vector>

#include "config/configuration.hh"

void Configuration::add_lib(const std::string& name) {
    _libs.emplace_back(name);
}

void Configuration::add_cube(const Cube& cube) {
    _cubes.emplace_back(cube);
}

std::ostream& operator<<(std::ostream& o_stream, const Configuration& config) {
    o_stream << "Read configuration:\n";
    o_stream << "  Libs:\n";
    for (auto&& lib: config._libs) {
        o_stream << "    Library name: " << lib << "\n";
    }

    o_stream << "  Cubes:\n";
    for (auto&& cube: config._cubes) {
        o_stream << "    Cube: " << cube.name << "\n";
        o_stream << "      Shift: " << cube.shift << "\n";
        o_stream << "      Scale: " << cube.scale << "\n";
        o_stream << "      Rotation: " << cube.rotation_deg << "\n";
        o_stream << "      Translation: " << cube.translation_m << "\n";
        o_stream << "      Color: " << cube.color_rgb << "\n";
    }

    return o_stream;
}

Vector3D Configuration::read_vector_parameters(std::istringstream& stream) {
    double x, y, z;
    stream >> x >> y >> z;
    if (stream.fail()) {
        throw std::runtime_error("Error reading Vector3D parameters");
    }

    Vector3D result;
    result[0] = x;
    result[1] = y;
    result[2] = z;

    return result;
}
