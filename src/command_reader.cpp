#include <algorithm>
#include <cstdio>
#include <filesystem>
#include <locale>
#include <sstream>

#include "command_reader.hh"

constexpr int LINE_SIZE = 500;

void CommandReader::read_commands(const std::filesystem::path& path, std::stringstream& out) {
    char line[LINE_SIZE];
    std::string preprocessor_command = "cpp -P -xc++ " + path.string();  // Intentional no space after -x

    FILE* process = popen(preprocessor_command.c_str(), "r");
    if (!process) {
        throw std::runtime_error("Error opening preprocessor process.");
    }

    while (fgets(line, LINE_SIZE, process)) {
        out << line;
    }

    if (pclose(process) != 0) {
        throw std::runtime_error("Error closing preprocessor process.");
    }
}