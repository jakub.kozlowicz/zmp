#include <chrono>
#include <filesystem>
#include <iostream>
#include <memory>
#include <set>
#include <thread>
#include <utility>

#include "command_reader.hh"
#include "communication/credentials.hh"
#include "communication/sender.hh"
#include "config/config_reader.hh"
#include "config/configuration.hh"
#include "plugin/plugin_loader.hh"

namespace fs = std::filesystem;

int main(int argc, char* argv[]) {
    if (argc != 3) {
        std::cerr << "Wrong usage of program!\n";
        std::cerr << "Example usage: " << argv[0] << "<file_with_commands> <config_file>\n";
        exit(-1);
    }

    fs::path commands_file(argv[1]), config_file(argv[2]);

    Configuration config;
    std::shared_ptr<Scene> scene = std::make_shared<Scene>();
    std::shared_ptr<Sender> sender = std::make_shared<Sender>(SERVER_ADDRESS, PORT);

    ConfigReader::parse_file(config_file.c_str(), config);
    std::cout << config << std::endl;

    const std::set<std::string> libraries(config.libs().begin(), config.libs().end());
    auto plugins = PluginLoader();

    if (!sender->open_connection()) {
        std::cerr << "Error open connection\n";
        exit(-1);
    }

    for (auto&& cube: config.cubes()) {
        std::ostringstream ss;
        ss << "AddObj Name=" << cube.name;
        ss << " RGB=" << cube.color_rgb;
        ss << " Scale=" << cube.scale;
        ss << " Shift=" << cube.shift;
        ss << " RotXYZ_deg=" << cube.rotation_deg;
        ss << " Trans_m=" << cube.translation_m;
        ss << "\n";

        try {
            sender->send(ss.str().c_str());
        } catch (const std::runtime_error& e) {
            std::cerr << e.what() << "\n";
            exit(-1);
        }

        std::shared_ptr<MobileObject> object = std::make_shared<MobileObject>();
        object->set_name(cube.name.c_str());
        object->set_position_m(cube.translation_m);
        object->set_angles_deg(cube.rotation_deg);

        scene->add_mobile_object(object);

        object.reset();
    }

    std::this_thread::sleep_for(std::chrono::seconds(2));

    try {
        std::stringstream ss;
        std::string command;
        CommandReader::read_commands(commands_file, ss);

        std::vector<std::thread> threads;
        while (ss >> command) {
            if (command != "Begin_Parallel_Actions") {
                continue;
            }

            while (command != "End_Parallel_Actions") {
                ss >> command;
                if ((libraries.find(command) == libraries.end())) {
                    continue;
                }

                auto command_interface = plugins.request(command);
                if (command_interface->read_params(ss)) {
                    threads.emplace_back([command_interface, scene, sender]() {
                        command_interface->print_syntax();
                        command_interface->print_command();
                        if (!command_interface->exec_command(scene, sender)) {
                            std::cerr << "Error executing command on object!" << std::endl;
                        }
                    });
                }
            }

            for (auto&& thread: threads) {
                if (thread.joinable()) {
                    thread.join();
                }
            }
            threads.clear();
        }
    } catch (const std::runtime_error& e) {
        std::cerr << "Error reading commands from preprocessor " << e.what() << "\n";
        exit(-1);
    }

    std::this_thread::sleep_for(std::chrono::seconds(1));

    try {
        sender->send("Clear\n");
        sender->send("Close\n");
    } catch (const std::runtime_error& e) {
        std::cerr << e.what() << "\n";
        exit(-1);
    }

    if (!sender->close_connection()) {
        std::cerr << "Error closing connection\n";
        exit(-1);
    }

    try {
        plugins.unload();
    } catch (const std::runtime_error& e) {
        std::cerr << "Error unloading shared object " << e.what() << "\n";
        exit(-1);
    }
}
