#ifndef SCENE_HH
#define SCENE_HH

#include <map>
#include <memory>
#include <mutex>
#include <vector>

#include "mobile_object.hh"
#include "vector_3D.hh"

class Scene {
    std::map<std::string, std::shared_ptr<MobileObject>> _objects;
    std::mutex _guard;

public:
    [[nodiscard]] std::shared_ptr<MobileObject> find_mobile_object(const std::string& object_name);
    void add_mobile_object(const std::shared_ptr<MobileObject>& mobile_object);
};

#endif /* SCENE_HH */