#ifndef INTERPRETER_COMMAND_HH
#define INTERPRETER_COMMAND_HH

#include <iostream>
#include <memory>

#include "mobile_object.hh"

#include "communication/sender.hh"
#include "scene/scene.hh"

class Interpreter4Command {
public:
    virtual void print_command() const = 0;

    virtual void print_syntax() const = 0;

    [[nodiscard]] virtual const char* get_command_name() const = 0;

    [[nodiscard]] virtual bool exec_command(std::shared_ptr<Scene> scene, std::shared_ptr<Sender> sender) const = 0;

    virtual bool read_params(std::istream& commands_stream) = 0;

    virtual ~Interpreter4Command() = default;
};

#endif /* INTERPRETER_COMMAND_HH */
