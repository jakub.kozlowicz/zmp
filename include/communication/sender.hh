#ifndef SENDER_HH
#define SENDER_HH

#include <mutex>
#include <string>

class Sender {
    int _socket_number;
    int _socket_fd;
    std::mutex _guard;
    const std::string _server_address;

public:
    Sender(std::string server_address, int socket_number);

    [[nodiscard]] bool open_connection();

    void send(const char* message);

    [[nodiscard]] bool close_connection();
};

#endif /* SENDER_HH */