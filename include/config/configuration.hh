#ifndef CONFIGURATION_HH
#define CONFIGURATION_HH

#include <iostream>
#include <list>
#include <map>
#include <sstream>
#include <string>

#include "vector_3D.hh"

class Configuration {
public:
    struct Cube {
        std::string name;
        Vector3D shift;
        Vector3D scale;
        Vector3D rotation_deg;
        Vector3D translation_m;
        Vector3D color_rgb;
    };

    void add_lib(const std::string& name);

    void add_cube(const Cube& cube);

    friend std::ostream& operator<<(std::ostream& o_stream, const Configuration& config);

    static Vector3D read_vector_parameters(std::istringstream& stream);

    const std::list<std::string>& libs() { return _libs; }

    const std::list<Cube>& cubes() { return _cubes; }

private:
    std::list<std::string> _libs;
    std::list<Cube> _cubes;
};

#endif /* CONFIGURATION_HH */
