#ifndef CONFIG_READER_HH
#define CONFIG_READER_HH

#include <string>

#include <xercesc/sax/Locator.hpp>
#include <xercesc/sax2/Attributes.hpp>
#include <xercesc/sax2/DefaultHandler.hpp>
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>

#include "config/configuration.hh"

class ConfigReader : public xercesc::DefaultHandler {
    Configuration& _config;

public:
    explicit ConfigReader(Configuration& config);

    void startDocument() override;

    void endDocument() override;

    void startElement(const XMLCh* uri, const XMLCh* local_name, const XMLCh* q_name,
                      const xercesc::Attributes& attributes) override;

    void endElement(const XMLCh* uri, const XMLCh* local_name, const XMLCh* q_name) override;

    void fatalError(const xercesc::SAXParseException& e) override;

    void error(const xercesc::SAXParseException& e) override;

    void warning(const xercesc::SAXParseException& e) override;

    void start_element(const std::string& element_name, const xercesc::Attributes& attributes);

    void process_lib_attributes(const xercesc::Attributes& attributes);

    void process_cube_attributes(const xercesc::Attributes& attributes);

    static void parse_file(const std::filesystem::path& config_file, Configuration& config);
};

#endif /* CONFIG_READER_HH */