#ifndef MOBILE_OBJECT_HH
#define MOBILE_OBJECT_HH

#include <mutex>
#include <string>

#include "vector_3D.hh"

class MobileObject {
    Vector3D _angles_deg;
    Vector3D _position_m;
    std::string _name;
    std::mutex _guard;

public:
    [[nodiscard]] double get_roll_angle_deg() const { return _angles_deg[0]; }

    [[nodiscard]] double get_pitch_angle_deg() const { return _angles_deg[1]; }

    [[nodiscard]] double get_yaw_angle_deg() const { return _angles_deg[2]; }

    void set_roll_angle_deg(double angle_deg) { _angles_deg[0] = angle_deg; }

    void set_pitch_angle_deg(double angle_deg) { _angles_deg[1] = angle_deg; }

    void set_yaw_angle_deg(double angle_deg) { _angles_deg[2] = angle_deg; }

    [[nodiscard]] const Vector3D& get_angles_deg() const { return _angles_deg; }

    Vector3D& use_angles_deg() { return _angles_deg; }

    void set_angles_deg(const Vector3D& angles_deg) { _angles_deg = angles_deg; }

    [[nodiscard]] const Vector3D& get_position_m() const { return _position_m; }

    Vector3D& use_position_m() { return _position_m; }

    void set_position_m(const Vector3D& position_m) { _position_m = position_m; }

    void set_name(const char* name) { _name = name; }

    [[nodiscard]] const std::string& get_name() const { return _name; }

    void lock() { _guard.lock(); }

    void unlock() { _guard.unlock(); }
};

#endif /* MOBILE_OBJECT_HH */
