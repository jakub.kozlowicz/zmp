#ifndef PLUGIN_INTERFACE_HH
#define PLUGIN_INTERFACE_HH

#include <memory>
#include <string>
#include <utility>

#include "interpreter_command.hh"

#include "communication/sender.hh"
#include "scene/scene.hh"

class PluginInterface {
    std::string _command_name;
    std::string _library_name;
    void* _library_handle;
    std::shared_ptr<Interpreter4Command> _command_interpreter;

public:
    explicit PluginInterface(std::string command_name);

    void load_plugin();

    void unload_plugin();

    void set_new_command(std::string command_name);

    std::string get_current_command();

    [[nodiscard]] const char* get_command_name() const { return _command_interpreter->get_command_name(); }

    void print_syntax() const { _command_interpreter->print_syntax(); }

    void print_command() const { _command_interpreter->print_command(); }

    [[nodiscard]] bool exec_command(std::shared_ptr<Scene> scene, std::shared_ptr<Sender> sender) const {
        return _command_interpreter->exec_command(std::move(scene), std::move(sender));
    }

    bool read_params(std::istream& commands_stream) { return _command_interpreter->read_params(commands_stream); }

    ~PluginInterface() = default;
};

#endif /* PLUGIN_INTERFACE_HH */