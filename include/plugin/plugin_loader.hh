#ifndef PLUGINS_LOADER_HH
#define PLUGINS_LOADER_HH

#include <memory>
#include <string>
#include <vector>

#include "plugin/plugin_interface.hh"

class PluginLoader {
    std::vector<std::shared_ptr<PluginInterface>> _collection;

public:
    std::shared_ptr<PluginInterface> request(const std::string& command_name);

    void unload();
};

#endif /* PLUGINS_LOADER_HH */
