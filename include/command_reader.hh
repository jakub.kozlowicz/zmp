#ifndef COMMAND_READER_HH
#define COMMAND_READER_HH

#include <filesystem>
#include <sstream>
#include <string>
#include <vector>

class CommandReader {
public:
    static void read_commands(const std::filesystem::path& path, std::stringstream& out);
};

#endif /* COMMAND_READER_HH */
