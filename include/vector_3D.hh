#ifndef VECTOR_3D_HH
#define VECTOR_3D_HH

#include "geomVector.hh"

typedef geom::Vector<double, 3> Vector3D;

#endif /* VECTOR_3D_HH */
